package it.ats.fsproject.pentaho;

import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.springframework.boot.logging.LogLevel;

public class PentahoTest {
	  public static void main(String[] args) throws Exception {
	    String filename = "/home/gvorster/tmp/pentaho/trans1.ktr";


	    try {
	        KettleEnvironment.init();


	        TransMeta transMeta = new TransMeta(filename);
	        Trans trans = new Trans(transMeta);


	        trans.setLogLevel(LogLevel.DETAILED);


	        trans.execute(null); // You can pass arguments instead of null.


	        trans.waitUntilFinished();


	        if ( trans.getErrors() > 0 )
	        {
	          throw new RuntimeException( "There were errors during transformation execution." );
	        }


	        System.out.println("stop");
	      }
	      catch ( KettleException e ) {
	        // TODO Put your exception-handling code here.
	        System.out.println(e);
	      }


	  }
	}

