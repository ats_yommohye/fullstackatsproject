package it.ats.FullStackProject.pentaho;

import java.io.FileOutputStream;
import java.io.IOException;

import org.pentaho.di.core.Const;
import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleXMLException;
import org.pentaho.di.core.util.EnvUtil;
import org.pentaho.di.core.xml.XMLHandler;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;

public class PentahoTransformation 
{
	public static void runTransformation (String transName) throws KettleXMLException 
	{
		try 
		{
			String directory="/Users/yomna/Desktop";
			KettleEnvironment.init();

			//StepLoader.init();
			EnvUtil.environmentInit();

			TransMeta metaData = new TransMeta(directory+transName);

			Trans trans = new Trans( metaData );
			System.out.println("4");
			trans.execute(new String[0]);

			trans.waitUntilFinished();

			System.out.println("6");
			if ( trans.getErrors() > 0 ) {
				System.out.print( "Error Executing transformation" );
			}

		} 
		catch (KettleException e) 
		{

			e.printStackTrace();
		}

		FileOutputStream fos = null;
		String filename=transName.substring(0,transName.length()-4)+"_report.xml" ;
		try 
		{
			fos = new FileOutputStream(filename );
			fos.write( XMLHandler.getXMLHeader().getBytes( Const.XML_ENCODING ) );
			//fos.write( metaData.getXml().getBytes( Const.XML_ENCODING ) );
		} 
		catch ( Exception e )
		{
			throw new KettleXMLException( "Unable to save to XML file '" + filename + "'", e );
		} finally {
			if ( fos != null ) {
				try {
					fos.close();
				} catch ( IOException e ) {
					throw new KettleXMLException( "Unable to close file '" + filename + "'", e );
				}
			}
		}
	}

}

