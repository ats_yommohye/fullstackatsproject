package it.ats.FullStackProject.controllers;

import org.pentaho.di.core.exception.KettleXMLException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import it.ats.FullStackProject.pentaho.PentahoTransformation;

@RestController
public class ProjectController 
{
	
	@RequestMapping("/")
	public ModelAndView index () {
		//using a ModelAndView object to be able to return an html page. 
	    ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("Index");
	    return modelAndView;
	}
	
	//when calling the URI below, the transformation will be executed 
	@RequestMapping(value = "/RunTrans/{transName}", method = RequestMethod.GET, produces = "application/json")
	public String runTransfromation(@PathVariable ("transName") String transName) 
	{
		try {
			PentahoTransformation.runTransformation(transName);
			return "ok";
		} catch (KettleXMLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "ko";
		}
		
	}
	
	
	
}
